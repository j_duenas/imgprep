import tinify
import glob
import os
from shutil import rmtree
from dotenv import load_dotenv, find_dotenv


# Setup variables
load_dotenv(find_dotenv(), override=True)
tinify.key = os.environ.get("API_KEY")
file_list = []
image_types = ["*.jpg", "*.png", "*.jpeg"]
tmp_dir = os.environ.get("TMP_DIR")
api_limit = int(os.environ.get("API_LIMIT"))


# If temporary directory doesn't exist, create it.
if not os.path.exists(tmp_dir):
    os.makedirs(tmp_dir)

# Looks for all files in current directory matching the file types specified
for image_type in image_types:
    file_list.extend(glob.glob(image_type))

# Iterate through file list and compress/resize
for file in file_list:
    # Resize photo
    print("Resizing " + file + "...")
    original_file = tinify.from_file(file)
    resized_file = original_file.resize(
        method="scale",
        width=1200
    )
    tmp_file = tmp_dir + "/_" + file
    resized_file.to_file(tmp_file)

    # Take resized photo and compress it
    print("Compressing " + file + "...")
    compressed = tinify.from_file(tmp_file)
    compressed.to_file("optimized__" + file)
    print("done")

rmtree(tmp_dir)
print("Compressions remaining this month: " + str(api_limit - tinify.compression_count))
